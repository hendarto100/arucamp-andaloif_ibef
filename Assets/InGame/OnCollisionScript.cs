﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class OnCollisionScript : MonoBehaviour
{
    void OnTriggerEnter(Collider collider){
        if(collider.gameObject.tag == "Fire"){
            LoadSceneFail();
        }else if(collider.gameObject.tag == "EmberAir"){
            LoadSceneSuccess();
        }
    }
    public void LoadSceneFail(){
        SceneManager.LoadScene("FailScene");
    }
    public void LoadSceneSuccess(){
        SceneManager.LoadScene("SuccessScene");
    }

}
